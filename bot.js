const Discord = require("discord.js");
const client = new Discord.Client();
const auth = require("./auth.json");

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", message => {
  if (message.content.startsWith("!role help")) {
    helpResponse(message);
  } else if (message.content.startsWith("!role suh")) {
    suhGif(message);
  } else if (message.content.startsWith("!role remove")) {
    manageRoles(message, "remove");
  } else if (message.content.startsWith("!role add", 0)) {
    manageRoles(message, "add");
  } else if (message.content.startsWith("!role list")) {
    listRoles(message);
  }
});

function helpResponse(message) {
  message.channel.send(
    "To use me simply use my command, state if you wish to remove or add roles, and provide the roles you would like. EX `!role add dnd` or `!role remove country music`. `!role list` will provide the avilable roles."
  );
}

function manageRoles(message, action) {
  let args = message.content.split(" ")
  args = args.splice(2);
  args.forEach(roleItem => {
    let assignableRole = message.guild.roles.cache.find(role => role.name === roleItem);
    if (assignableRole) {
      if (action === "add") {
        message.member.roles.add(assignableRole).catch(console.error);
      } else {
        message.member.roles.remove(assignableRole).catch(console.error);
      }
    }
  })
  message.channel.send("Your roles have been updated");
}

function listRoles(message) {
  message.guild.roles.fetch()
  .then(roles => {
    let theRoles =  roles.cache.map(r => r.name.match(/^(@everyone|admin|mod|rolethane)$/) ? null : r.name).join('\n');
    message.channel.send("The following roles are available: " + theRoles);
  })
  .catch(console.error)
}

function suhGif(message) {
  let suh = [
    "https://gfycat.com/agededucatedblesbok",
    "https://media.giphy.com/media/QZF6K7YEPhPNK/giphy.gif"
  ];
  message.channel.send(suh[Math.floor(Math.random() * suh.length)]);
}

client.login(auth.token);
